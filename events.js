var EVENTER = (function(){
	
	'use strict'

  	var pref_   = "eventer" + new Date().getTime(),
  	    ev_     = {},
        storage = {};


	/**
	 * Is it an array
	 * @param o checking element
	 */
	var isArray = function(o){
		return o.constructor === Array;
	};


	/**
	 * Is it an Object
	 * @param o checking element
	 */
	var isObject = function(o){
		return o.constructor === Object;
	};


	/**
	 * Is it a String
	 * @param o checking element
	 */
	var isString = function(o){
		return o.constructor === String;
	};


    /* Set event */
    var set = function(e, o, f){
      if ( o[pref_][e] ) {
         o[pref_][e].push(f);
      } else {
         o[pref_][e] = [f];
      };
      ev_[e] ? ev_[e].push(f) : ev_[e] = [f];
    };	  


    /* Add event to object */	
    var add = function (o, e, f) {
      if ( !o[pref_] ) o[pref_] = {};

      if( isString(e) ) {
          set(e, o , f);
      } else if ( isArray(e) ) {
          for( var i = 0; i < e.length; i++ ){
              set(e[i], o, f);
          };
      }; 
    };

    /** 
     * Call all handlers 
     * @param {Array} array of handlers
     */
    var callF = function (a, p) {
      if ( a ) {
        for ( var i = 0; i < a.length; i++ ) {
          if ( a[i] ) a[i](p || null);
        };
      };
    };

    /** 
     * Call handlers from objects array 
     * @param {Object} object
     */
    var callFArray = function(o, e, p){
        if( isString(e) ){
            callF(o[e], p);
        }else if( isArray(e) ){
            for( var ev = 0; ev < e.length; ev++ ){
                callF(o[e[ev]], p);
            };
        };
    };



	return {

        events: storage,

		/**
		 * Method bind event on object
		 * @param {Object} our object
		 * @param {String} event name
		 * @param {Function} handler
		 */
		on: function (o, e, f) {
            var ln = o.length;

			if( isArray(o) ){
			  for( var i = 0; i < ln; i++ ){
				add(o[i], e, f);
			  };
			} else if( isObject(o) ) {
			  add(o, e, f);
			}else if( isString(o) ) {
			  add(storage, arguments[0], arguments[1]);
            };
		},


		/**
		 * Method emit the event
		 * @param {Object} our object
		 * @param {String} event name
		 * @param p parameters 
		 */
		trigger: function(o, e, p){

			/* If first parameter is object */
			if( isObject(o) ) {
				if ( o[pref_] ) callFArray(o[pref_], e, p);
			} 

			/* If first parameter is Array */
			else if ( isArray(o) && arguments.length > 1 ) {
			  for ( var i = 0; i < o.length; i++ ) {
				if ( o[i][pref_] ) callFArray(o[i][pref_], e, p);	
			  };
			}

			/* If we want to call this event from all objects */
			else if( isString(o) ) {
				for( var j = 0; j < ev_[o].length; j++ ){
					ev_[o][j](e || null);
				};  
			} 

			/* If we eant to call an array of events from all objects */
			else if ( isArray(o) && 1 === arguments.length) {
				
				//Loop all events
				for( var eo = 0; eo < o.length; eo++ ){

					//Loop all objects
					for( var eo_ = 0; eo_ < ev_[o[eo]].length; eo_++ ){
						ev_[o[eo]][eo_](e || null);
					}; 
				};
			};
		}
	};
}());






